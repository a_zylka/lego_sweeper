"""
@file load.py
@date July 2017
@author Adam Zylka
contains some loading functions
"""


import yaml


def load_yaml_config_parameters(path):
    """
    loads config parameters from a given yaml file
    @param path: path to config file
    @return: parameters
    """

    parameters = None
    with open(path, 'r') as fp:
        try:
            parameters = yaml.load(fp)
        except yaml.YAMLError as err:
            print(err)
            return None

    return parameters

