#!/usr/bin/env python
"""
@file monitor_battery.py
@autor Adam Zylka
@date July 2018
assign motors in constructor
"""


from __future__ import division

import rospy
import brickpi3 as bp3
from lego_sweeper.srv import drive as drive_srv, stop as stop_srv
from lib_load import load_yaml_config_parameters
from std_msgs.msg import Bool, Int32


# constants
LOG_LEVEL = rospy.DEBUG
SPEED = 1


class Drive:
    """
    BatterDrive
    drives robot
    """

    def __init__(self, param):
        """
        constructor
        sets up ros node, publisher and brickpi3
        @param param: parameters from config file
        """

        # ros
        self.__node_name = param['node_name']
        rospy.init_node(self.__node_name, anonymous=True,
                log_level=LOG_LEVEL)
        rospy.loginfo('setting up %s' % (self.__node_name))
        self.__srv_drive = rospy.Service(param['srv_name_drive'], drive_srv,
                self.__drive_srv)
        self.__srv_stop = rospy.Service(param['srv_name_stop'], stop_srv,
                self.__stop_srv)
        self.__rate = rospy.Rate(param['run_rate_hz'])

        # lego
        self.__bp3 = bp3.BrickPi3()
        # can not be moved to config file
        self.__motors = {'left': self.__bp3.PORT_A, 'right': self.__bp3.PORT_D}

        # variables
        self.__stop = True
        self.__drive_directions = param['drive_directions']
        self.__min_speed = param['min_speed']
        self.__max_speed = param['max_speed']
        self.__direction = ''
        self.__speed = 0
        self.__speed_prev = 0

        rospy.loginfo('finished setup of %s' % (self.__node_name))

    def __del__(self):
        """
        destructor
        cleans up brickpi3 settings
        """

        self.__bp3.reset_all()

    def setup_motors(self):
        """
        reset encoder offset
        @return: True if ok else False
        """

        try:
            for motor in self.__motors.values():
                self.__bp3.offset_motor_encoder(motor,
                        self.__bp3.get_motor_encoder(motor))

        except IOError as error:
            rospy.logerr(error)
            return False

        return True

    def __check_param_drive_srv(self, srv):
        if srv.direction.lower() in self.__drive_directions and \
                self.__min_speed <= srv.speed <= self.__max_speed:
            return True

        return False

    def __drive_srv(self, srv):
        """
        checks input parameters and if true sets variables
        @param srv: drive.srv
        @return: True if srv ok else Fales
        """

        rospy.logdebug('drive_srv received srv:')
        rospy.logdebug('direction: %s' % (srv.direction))
        rospy.logdebug('speed: %s' % (srv.speed))

        ret = self.__check_param_drive_srv(srv)
        if ret is False:
            return ret

        if self.__stop:
            self.__stop = False
        self.__direction = srv.direction
        self.__speed = srv.speed
        if self.__direction.lower() == 'backward':
            self.__speed = -srv.speed

        return ret

    def __stop_srv(self, srv):
        """
        stops robot
        @param srv: stop.srv
        @return: True
        """

        rospy.logdebug('stopping robot')
        self.__stop = True
        self.__direction = ''
        self.__speed = 0
        return True

    def __motion_control(self):

        for motor in self.__motors.values():
            status = self.__bp3.get_motor_status(motor)
            current_speed = status[SPEED]

            if current_speed < self.__speed:
                current_speed += 1
            elif self.__speed < current_speed:
                current_speed -= 1

            if current_speed > self.__max_speed:
                current_speed = self.__max_speed
            elif current_speed < -self.__max_speed:
                current_speed = -self.__max_speed
            self.__speed_prev = current_speed

            self.__bp3.set_motor_power(motor, current_speed)

            rospy.logdebug('desired_speed: %s \t current_speed: %s' % \
                    (self.__speed, self.__speed_prev))
        rospy.logdebug('')

    def run(self):

        while not rospy.is_shutdown():
            # rospy.loginfo('speed: %s' % (self.__speed))

            if self.__speed_prev != self.__speed:
                self.__motion_control()
            # for motor in self.__motors.values():
                # self.__bp3.set_motor_power(motor, self.__speed)

            # sleep
            self.__rate.sleep()

        # stop motors
        for motor in self.__motors.values():
            self.__bp3.set_motor_power(motor, 0)


def main(path_config_file):
    """
    load parameter from config file and run some drive functions
    """

    param = load_yaml_config_parameters(path_config_file)
    if param is None:
        print('config file: %s not found' % (path_config_file))
        return

    try:
        D = Drive(param)
        if not D.setup_motors():
            return

        D.run()

    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    """
    run main
    """

    # constants
    PATH_PROJECT = '/home/adam/catkin_ws/src/lego_sweeper'
    # PATH_PROJECT = '/home/adam/programming/ros_lunar_ws/src/lego_sweeper'
    PATH_CONFIG_FILE = PATH_PROJECT + '/config/drive.yaml'

    main(PATH_CONFIG_FILE)

