#!/usr/bin/env python
"""
@file monitor_battery.py
@autor Adam Zylka
@date July 2018
"""


import rospy
import brickpi3 as bp3
from lib_load import load_yaml_config_parameters
from std_msgs.msg import Bool, Int32


# constants
LOG_LEVEL = rospy.INFO


class BatteryMonitor:
    """
    BatteryMonitor
    checks battery voltage and if voltage is high engough, allows robot to
    work
    """

    def __init__(self, param):
        """
        constructor
        sets up ros node, publisher and brickpi3
        @param param: parameters from config file
        """

        # ros
        self.__node_name = param['node_name']
        rospy.init_node(self.__node_name, anonymous=True,
                log_level=LOG_LEVEL)
        rospy.loginfo('setting up %s' % (self.__node_name))
        rospy.logdebug('param: %s' % (param))
        self.__pub_volt = rospy.Publisher(param['pub_name_volt'], Int32,
                queue_size=param['pub_queue_size'])
        self.__pub_run = rospy.Publisher(param['pub_name_run'], Bool,
                queue_size=param['pub_queue_size'])
        self.__rate = rospy.Rate(param['run_rate_hz'])
        # lego
        self.__bp3 = bp3.BrickPi3()
        # variables
        self.__battery_threshold = param['battery_threshold']
        rospy.loginfo('finished setup of %s' % (self.__node_name))

    def __del__(self):
        """
        destructor
        cleans up brickpi3 settings
        """

        self.__bp3.reset_all()

    def run(self):

        while not rospy.is_shutdown():
            # get raw_voltage
            raw_voltage = self.__bp3.get_voltage_battery()
            rospy.logdebug(raw_voltage)

            # round
            rounded_voltage = int(round(raw_voltage))
            rospy.logdebug(rounded_voltage)
            self.__pub_volt.publish(rounded_voltage)

            # check threshold
            if rounded_voltage > self.__battery_threshold:
                self.__pub_run.publish(True)
            else:
                self.__pub_run.publish(False)

            # sleep
            self.__rate.sleep()


def main(path_config_file):
    """
    load parameter from config file and run BatteryMonitor
    """

    param = load_yaml_config_parameters(path_config_file)
    if param is None:
        print('config file: %s not found' % (path_config_file))
        return

    try:
        BM = BatteryMonitor(param)
        BM.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    """
    run main
    """

    # constants
    PATH_PROJECT = '/home/adam/catkin_ws/src/lego_sweeper'
    PATH_CONFIG_FILE = PATH_PROJECT + '/config/monitor_battery.yaml'

    main(PATH_CONFIG_FILE)

